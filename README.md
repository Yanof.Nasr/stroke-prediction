# Stroke prediction

According to the World Health Organization (WHO) stroke is the 2nd leading cause of death globally, responsible for approximately 11% of total deaths.
This dataset is used to predict whether a patient is likely to get stroke based on the input parameters like gender, age, various diseases, and smoking status. Each row in the data provides relavant information about the patient.

![Stroke](https://i.makeagif.com/media/11-12-2015/kxoOxr.gif)

## Model NN Architecture
```
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_6 (InputLayer)           [(None, 21)]         0           []                               
                                                                                                  
 dense_35 (Dense)               (None, 600)          13200       ['input_6[0][0]']                
                                                                                                  
 dropout_30 (Dropout)           (None, 600)          0           ['dense_35[0][0]']               
                                                                                                  
 dense_36 (Dense)               (None, 128)          76928       ['dropout_30[0][0]']             
                                                                                                  
 batch_normalization_10 (BatchN  (None, 128)         512         ['dense_36[0][0]']               
 ormalization)                                                                                    
                                                                                                  
 dropout_31 (Dropout)           (None, 128)          0           ['batch_normalization_10[0][0]'] 
                                                                                                  
 dense_37 (Dense)               (None, 256)          33024       ['dropout_31[0][0]']             
                                                                                                  
 batch_normalization_11 (BatchN  (None, 256)         1024        ['dense_37[0][0]']               
 ormalization)                                                                                    
                                                                                                  
 dropout_32 (Dropout)           (None, 256)          0           ['batch_normalization_11[0][0]'] 
                                                                                                  
 dense_38 (Dense)               (None, 128)          32896       ['dropout_32[0][0]']             
...
Total params: 617,289
Trainable params: 615,001
Non-trainable params: 2,288
```
